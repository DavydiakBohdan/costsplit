﻿using Ardalis.Specification;
using CostSplitProject.Domain.Models;

namespace CostSplitProject.BusinessLogic.Specification
{
    public class PaymentWithUserSpecification : Specification<Payment>
    {
        public PaymentWithUserSpecification(int id)
        {
            Query.Where(x => x.Id == id).Include(x => x.ToUser);
        }
    }
}
