﻿using Ardalis.Specification;
using CostSplitProject.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.Specification
{
    public class GroupExpensesSpecification : Specification<ExpenseHeader>
    {
        public GroupExpensesSpecification(int id)
        {
            Query.Where(g=>g.GoupId == id).Include(x => x.ExpenseLines);
        }
    }
}
