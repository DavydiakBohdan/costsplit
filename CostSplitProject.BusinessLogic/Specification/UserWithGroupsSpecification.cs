﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using CostSplitProject.Domain.Models;

namespace CostSplitProject.Specification
{
    public class UserWithGroupsSpecification:Specification<User>
    {
        public UserWithGroupsSpecification(int Id)
        {
            Query.Where(u => u.Id == Id ).Include(u => u.Groups );
        }
    }
}
