﻿using Ardalis.Specification;
using CostSplitProject.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.Specification
{
    public class PaymentUserConfirmSpecification : Specification<Payment>
    {
        public PaymentUserConfirmSpecification(int userId,int groupId)
        {
            Query.Where(p =>  p.ToUserId == userId && p.GroupId == groupId && p.Confirmed);
        }
    }
}
