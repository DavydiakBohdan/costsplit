﻿using Ardalis.Specification;
using CostSplitProject.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.Specification
{
    public class GroupWithUsersSpecification : Specification<Group>
    {
        public GroupWithUsersSpecification(int Id)
        {
            Query.Where(g => g.Id == Id).Include(g => g.Users );
        }
    }
}
