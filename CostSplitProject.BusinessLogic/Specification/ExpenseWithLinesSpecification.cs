﻿using Ardalis.Specification;
using CostSplitProject.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitProject.BusinessLogic.Specification
{
    class ExpenseWithLinesSpecification : Specification<ExpenseHeader>
    {
        public ExpenseWithLinesSpecification(int id)
        {
            Query.Where(x => x.Id == id)
                .Include(x => x.ExpenseLines)
                .ThenInclude(x => x.User)
                .Include(x => x.Group)
                .Include(x => x.User);
        }
    }
}
