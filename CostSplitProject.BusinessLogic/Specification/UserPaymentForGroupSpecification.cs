﻿using Ardalis.Specification;
using CostSplitProject.Domain.Models;
using System.Linq;

namespace CostSplitProject.Specification
{
    public class UserPaymentForGroupSpecification:Specification<Payment>
    {
        public UserPaymentForGroupSpecification(int userId,int groupId)
        {
            Query.Where(p => (p.FromUserId == userId || p.ToUserId == userId)&&p.GroupId == groupId)
                .Include(x => x.Group).Include(x => x.ToUser);
        }
    }
}
