﻿using CostSplitProject.BusinessLogic.Exceptions;
using CostSplitProject.BusinessLogic.Services.Interfaces;
using CostSplitProject.BusinessLogic.Specification;
using CostSplitProject.DataAccess.Repositories.interfaces;
using CostSplitProject.Domain.Models;
using CostSplitProject.Specification;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.BusinessLogic.Services.Implementation
{
    public class ExpenseService : IExpenseService
    {
        private readonly IExpenseHeaderRepository _expenseRepository;
        private readonly IGroupRepository _groupRepository;
        private readonly IPaymentService _paymentService;
        public ExpenseService(IExpenseHeaderRepository expenseRepository,
            IGroupRepository groupRepository, IPaymentService paymentService)
        {
            _paymentService = paymentService;
            _expenseRepository = expenseRepository;
            _groupRepository = groupRepository;
        }

        public async Task<ExpenseHeader> CreateExpenseAsync(ExpenseHeader expense,decimal totalAmount, ICollection<User> users)
        {
            await ExpenseValidation(expense, users);

            decimal amount = totalAmount / users.Count();
            expense.ExpenseLines = new List<ExpenseLine>();
            foreach (var user in users.Where(x => x.Id != expense.UserId))
            {
                expense.ExpenseLines.Add(new ExpenseLine() {
                    UserId = user.Id,
                    Amount = amount
                });
            }
            var result = await _expenseRepository.InsertAsync(expense);
            await _expenseRepository.UnitOfWork.SaveChangesAsync();
            return result;
        }

        private async Task ExpenseValidation(ExpenseHeader expense, ICollection<User> users)
        {
            if (expense is null)
            {
                throw new ExpenseValidationError();
            }
            var group = await _groupRepository.GetOneAsync(new GroupWithUsersSpecification(expense.GoupId));
            if (group is null)
            {
                throw new GroupDoesNotExist();
            }
            if (!(group.Users.Select(x => x.Id).All(users.Select(x => x.Id).Contains)))
            {
                throw new ExpenseValidationError();
            }
        }

        public async Task<ExpenseHeader> GetExpenseAsync(int id)
        {
            var specification = new ExpenseWithLinesSpecification(id);
            var expense = await _expenseRepository.GetOneAsync(specification);
            if (expense is null)
            {
                throw new ExpenseDoesNotExist();
            }
            return expense;
        }

        public async Task<ICollection<ExpenseHeader>> GetExpenseForGroupAsync(int groupId)
        {
            var group = await _groupRepository.GetByIdAsync(groupId);
            if (group is null)
            {
                throw new GroupDoesNotExist();
            }
            var specefication = new GroupExpensesSpecification(groupId);
            return await _expenseRepository.GetManyAsync(specefication);
        }



        public async Task<Debt> CalculateDebtAsync(int groupId, int userId, int toUserId) 
        {
            var group = await _groupRepository.GetByIdAsync(groupId);
            if (group is null)
            {
                throw new GroupDoesNotExist();
            }
            Debt debt = new Debt()
            {
                UserId = userId,
                Amount = 0,
                ToUserId = toUserId
            };
            await CalculateDebtFromPayments(groupId, userId, toUserId, debt);
            await CalculateDebtFromExpenses(groupId, userId, toUserId, debt);
            return debt;
        }

        
        private async Task CalculateDebtFromPayments(int groupId, int userId, int toUserId, Debt debt)
        {
            var payments = await _paymentService.GetUserConfirmedPaymentsInGroupAsync(groupId, userId);
            if(payments is null) 
            {
                return ;
            }
            foreach (var payment in payments)
            {
                if (payment.FromUserId == userId && payment.ToUserId == toUserId)
                {
                    debt.Amount += payment.Amount;
                }

                if (payment.FromUserId == toUserId && payment.ToUserId == userId)
                {
                    debt.Amount -= payment.Amount;
                }
            }
        }

        private async Task CalculateDebtFromExpenses(int groupId, int userId, int toUserId, Debt debt)
        {        
            var expenses = await GetExpenseForGroupAsync(groupId);
            if(expenses is null)
            {
                return;
            }
            foreach (var expense in expenses)
            {
                if (expense.UserId == userId)
                {
                    var expenseLine = expense.ExpenseLines.FirstOrDefault(x => x.UserId == toUserId);
                    if (expenseLine != null)
                    {
                        debt.Amount += expenseLine.Amount;
                    }
                }

                if (expense.UserId == toUserId)
                {
                    var expenseLine = expense.ExpenseLines.FirstOrDefault(x => x.UserId == userId);
                    if (expenseLine != null)
                    {
                        debt.Amount -= expenseLine.Amount;
                    }
                }
            }
        }

    }
}
