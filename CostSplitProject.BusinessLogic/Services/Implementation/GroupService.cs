﻿using Ardalis.Specification;
using CostSplitProject.BusinessLogic.Exceptions;
using CostSplitProject.BusinessLogic.Services.Interfaces;
using CostSplitProject.DataAccess.Repositories.interfaces;
using CostSplitProject.Domain.Models;
using CostSplitProject.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitProject.BusinessLogic.Services.Implementation
{
    public class GroupService : IGroupService
    {
        private readonly IGroupRepository _repositoryGroups;
        private readonly IUserRepository _repositoryUsers;
        public GroupService(IGroupRepository groupRepository, IUserRepository usersRepository)
        {
            _repositoryGroups = groupRepository;
            _repositoryUsers = usersRepository;
        }
        public async Task AddUserAsync(int userId, int groupId)
        {
            var group = await _repositoryGroups.GetByIdAsync(groupId);
            var user = await _repositoryUsers.GetByIdAsync(userId);
            group.Users.Add(user);
            await _repositoryGroups.UpdateAsync(group);
            await _repositoryGroups.UnitOfWork.SaveChangesAsync();
        }

        public async Task<Group> CreareGroup(Group group)
        {
            var result = await _repositoryGroups.InsertAsync(group);
            await _repositoryGroups.UnitOfWork.SaveChangesAsync();
            return result;
        }

        public async Task<ICollection<User>> GetUsersAsync(int gropId)
        {
            var specefication = new GroupWithUsersSpecification(gropId);
            var group = await _repositoryGroups.GetOneAsync(specefication);
            if (group is null)
            {
                throw new GroupDoesNotExist();
            }
            return group.Users.ToList();
        }

    }
}
