﻿using CostSplitProject.BusinessLogic.Exceptions;
using CostSplitProject.BusinessLogic.Services.Interfaces;
using CostSplitProject.DataAccess.Repositories.interfaces;
using CostSplitProject.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitProject.BusinessLogic.Services.Implementation
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User> GetUserAsync(int id)
        {
            return await _userRepository.GetByIdAsync(id);
        }

        public async Task<User> AuthenticateUserAsync(string login, string password)
        {
            var user = await _userRepository.GetByNameAsync(login);
            if (user.Password.Equals(GetHashString(password))) 
            {
                return user;
            }
            return null;
        }

        public async Task<User> RegisterUserAsync(string login, string password)
        {
            var user = await _userRepository.GetByNameAsync(login);
            if (user is not null)
            {
                throw new Exception();
            }
            user = await _userRepository.InsertAsync(new User
            {
                Name = login,
                Password = GetHashString(password)
            });
            await _userRepository.UnitOfWork.SaveChangesAsync();
            return user;
        }

        string GetHashString(string password)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(password);

            MD5CryptoServiceProvider CSP =
                new MD5CryptoServiceProvider();

            byte[] byteHash = CSP.ComputeHash(bytes);
            StringBuilder hash = new StringBuilder();

            foreach (byte b in byteHash)
                hash.Append(string.Format("{0:x2}", b ));

            return hash.ToString();
        }
    }
}
