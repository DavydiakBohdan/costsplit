﻿using Ardalis.Specification;
using CostSplitProject.BusinessLogic.Exceptions;
using CostSplitProject.BusinessLogic.Services.Interfaces;
using CostSplitProject.BusinessLogic.Specification;
using CostSplitProject.DataAccess.Repositories.interfaces;
using CostSplitProject.Domain.Models;
using CostSplitProject.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitProject.BusinessLogic.Services.Implementation
{
    public class PaymentService : IPaymentService
    {
        private readonly IPaymentRepository _paymentRepository;

        public PaymentService(IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository;
        }

        public async Task ConfirmPaymentAsync(int paymentId, int userId)
        {
            var payment = await _paymentRepository.GetOneAsync(new PaymentWithUserSpecification(paymentId));
            if (payment is null) 
            {
                throw new System.Exception();
            }
            if (payment.Confirmed)
            {
                return ;
            }
            if (payment.ToUser.Id == userId)
            {
                payment.Confirmed = true;
                await _paymentRepository.UpdateAsync(payment);
                await _paymentRepository.UnitOfWork.SaveChangesAsync();
            }
        }

        public async Task<Payment> CreatePaymentAsyinc(Payment payment)
        {
            var result = await _paymentRepository.InsertAsync(payment);
            await _paymentRepository.UnitOfWork.SaveChangesAsync();
            return result;
        }

        public async Task<ICollection<Payment>> GetPaymentsInGroupAsync(int groupId, int userId)
        {
            var specification = new UserPaymentForGroupSpecification(userId, groupId);
            var payment = await _paymentRepository.GetManyAsync(specification);
            return payment;
        }

        public async Task<ICollection<Payment>> GetUserConfirmedPaymentsInGroupAsync(int groupId, int userId)
        {
            var specification = new PaymentUserConfirmSpecification(userId, groupId);
            var payment = await _paymentRepository.GetManyAsync(specification);
            return payment;
        }
    }
}
