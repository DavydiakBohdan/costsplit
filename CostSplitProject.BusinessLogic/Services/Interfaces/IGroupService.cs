﻿using Ardalis.Specification;
using CostSplitProject.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitProject.BusinessLogic.Services.Interfaces
{
    public interface IGroupService
    {
        Task<ICollection<User>> GetUsersAsync(int gropId);
        Task AddUserAsync(int userId, int groupId);
        Task<Group> CreareGroup(Group group);
    }
}
