﻿using Ardalis.Specification;
using CostSplitProject.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitProject.BusinessLogic.Services.Interfaces
{
    public interface IPaymentService
    {
        Task<ICollection<Payment>> GetPaymentsInGroupAsync(int groupId, int userId);
        Task<ICollection<Payment>> GetUserConfirmedPaymentsInGroupAsync(int groupId ,int userId);
        Task<Payment> CreatePaymentAsyinc(Payment payment);
        Task ConfirmPaymentAsync(int paymentId,int userId);
    }
}
