﻿using CostSplitProject.BusinessLogic.Services.Implementation;
using CostSplitProject.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitProject.BusinessLogic.Services.Interfaces
{
    public interface IExpenseService
    {
        Task<ExpenseHeader> CreateExpenseAsync(ExpenseHeader expense, decimal totalAmount, ICollection<User> users);
        Task<ExpenseHeader> GetExpenseAsync(int id);
        Task<ICollection<ExpenseHeader>> GetExpenseForGroupAsync(int groupId);
        Task<Debt> CalculateDebtAsync(int groupId, int userId, int toUserId);
    }
}
