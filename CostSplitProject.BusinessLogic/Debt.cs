﻿namespace CostSplitProject.BusinessLogic.Services.Implementation
{
    public  class Debt
    {
        public decimal Amount { get; set; }
        public int UserId { get; set; }
        public int ToUserId { get; set; }
    }
}