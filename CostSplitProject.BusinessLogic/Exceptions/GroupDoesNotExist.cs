﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace CostSplitProject.BusinessLogic.Exceptions
{
    public class GroupDoesNotExist:CustomException
    {
        public GroupDoesNotExist():base(HttpStatusCode.NotFound,"Group Does Not Exist")
        {

        }
    }
}
