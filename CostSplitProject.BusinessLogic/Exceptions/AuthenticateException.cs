﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitProject.BusinessLogic.Exceptions
{
    class AuthenticateException : CustomException
    {
        public AuthenticateException() : base(HttpStatusCode.Unauthorized, "Wrong login or password")
        {
        }
    }
}
