﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitProject.BusinessLogic.Exceptions
{
    class ExpenseValidationError : CustomException
    {
        public ExpenseValidationError() : base(HttpStatusCode.BadRequest, "Expense is not valid")
        {
        }
    }
}
