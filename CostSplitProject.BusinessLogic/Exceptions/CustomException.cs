﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace CostSplitProject.BusinessLogic.Exceptions
{
    public class CustomException : Exception
    {
        public HttpStatusCode StatusCode{ get; set;} 
        public CustomException(HttpStatusCode code, string message):base(message)
        {
            StatusCode = code;
        }
    }
}
