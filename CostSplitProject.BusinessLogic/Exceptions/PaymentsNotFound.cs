﻿using System.Net;

namespace CostSplitProject.BusinessLogic.Exceptions
{
    class PaymentsNotFound:CustomException
    {
        public PaymentsNotFound():base(HttpStatusCode.NotFound,"Payments not found")
        {

        }
    }
}
