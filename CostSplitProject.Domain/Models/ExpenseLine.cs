﻿using CostSplitProject.Domain.Interfaces;
using System.Collections.Generic;


namespace CostSplitProject.Domain.Models
{
    public class ExpenseLine:IEntity<int>
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public int ExpenseId { get; set; }
        public ExpenseHeader Expense { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

    }
}
