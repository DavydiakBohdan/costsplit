﻿using CostSplitProject.Domain.Interfaces;
using System;
using System.Collections.Generic;


namespace CostSplitProject.Domain.Models
{
    public class Payment : IEntity<int>
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; } = DateTime.UtcNow;
        public decimal Amount { get; set; }
        public bool Confirmed { get; set; }

        public int ToUserId { get; set; }
        public User ToUser { get; set; }

        public int FromUserId { get; set; }
        public User FromUser { get; set; }

        public int GroupId { get; set; }
        public Group Group { get; set; }
    }
}
