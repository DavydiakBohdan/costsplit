﻿using System.Collections.Generic;
using CostSplitProject.Domain.Interfaces;

namespace CostSplitProject.Domain.Models
{
    public class User : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Password { get; set; }
        public ICollection<Payment> ToUserPayments { get; set; }
        public ICollection<Payment> FromUserPayments { get; set; }
        public ICollection<Group> Groups { get; set; }
        public ICollection<ExpenseHeader> ExpenseHeaders { get; set; }
        public ICollection<ExpenseLine> ExpenseLines { get; set; }
    }
}
