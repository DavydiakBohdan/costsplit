﻿using CostSplitProject.Domain.Interfaces;
using System.Collections.Generic;


namespace CostSplitProject.Domain.Models
{
    public class Group : IEntity<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<User> Users { get; set; } = new List<User>();
        public ICollection<ExpenseHeader> ExpenseHeaders { get; set; }
        public ICollection<Payment> Payments { get; set; }
    }
}
