﻿using CostSplitProject.Domain.Interfaces;


namespace CostSplitProject.Domain.Models
{
    public class SimpleModel:IEntity<int>
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public int Age { get; set; }
    }
}
