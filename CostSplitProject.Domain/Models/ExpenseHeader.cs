﻿using System;
using System.Collections.Generic;
using CostSplitProject.Domain.Interfaces;

namespace CostSplitProject.Domain.Models
{
    public class ExpenseHeader:IEntity<int>
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; } = DateTime.UtcNow;

        public ICollection<ExpenseLine> ExpenseLines { get; set; } = new List<ExpenseLine>();

        public int UserId { get; set; }
        public User User { get; set; }

        public int GoupId { get; set; }
        public Group Group { get; set; }
    }
}
