﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CostSplitProject.DataAccess.Migrations
{
    public partial class SecondMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExpenseLines_ExpenseHeaders_ExpenseId",
                table: "ExpenseLines");

            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Groups_GroupId",
                table: "Payments");

            migrationBuilder.AddForeignKey(
                name: "FK_ExpenseLines_ExpenseHeaders_ExpenseId",
                table: "ExpenseLines",
                column: "ExpenseId",
                principalTable: "ExpenseHeaders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Groups_GroupId",
                table: "Payments",
                column: "GroupId",
                principalTable: "Groups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExpenseLines_ExpenseHeaders_ExpenseId",
                table: "ExpenseLines");

            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Groups_GroupId",
                table: "Payments");

            migrationBuilder.AddForeignKey(
                name: "FK_ExpenseLines_ExpenseHeaders_ExpenseId",
                table: "ExpenseLines",
                column: "ExpenseId",
                principalTable: "ExpenseHeaders",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Groups_GroupId",
                table: "Payments",
                column: "GroupId",
                principalTable: "Groups",
                principalColumn: "Id");
        }
    }
}
