﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CostSplitProject.DataAccess.Migrations
{
    public partial class TimeOnCreation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Payments",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 5, 31, 8, 28, 53, 411, DateTimeKind.Utc).AddTicks(6217),
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "ExpenseHeaders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 5, 31, 8, 28, 53, 432, DateTimeKind.Utc).AddTicks(671),
                oldClrType: typeof(DateTime),
                oldType: "datetime2");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "Payments",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 5, 31, 8, 28, 53, 411, DateTimeKind.Utc).AddTicks(6217));

            migrationBuilder.AlterColumn<DateTime>(
                name: "Time",
                table: "ExpenseHeaders",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 5, 31, 8, 28, 53, 432, DateTimeKind.Utc).AddTicks(671));
        }
    }
}
