﻿using Microsoft.EntityFrameworkCore;
using CostSplitProject.Domain.Models;
using CostSplitProject.DataAccess.Repositories.interfaces;
using System;

namespace CostSplitProject.DataAccess.Context
{
    public class SplitAppContext : DbContext ,IUnitOfWork
    {

        DbSet<User> Users { get; set; }
        DbSet<Payment> Payments { get; set; }
        DbSet<Group> Groups { get; set; }
        DbSet<ExpenseHeader> ExpenseHeaders { get; set; }
        DbSet<ExpenseLine> ExpenseLines { get; set; }


        public SplitAppContext(DbContextOptions<SplitAppContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Payment>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasOne(x => x.FromUser)
                    .WithMany(fu => fu.FromUserPayments)
                    .HasForeignKey(x => x.FromUserId).IsRequired()
                    .OnDelete(DeleteBehavior.NoAction);
                b.HasOne(x => x.ToUser)
                    .WithMany(tu => tu.ToUserPayments)
                    .HasForeignKey(x => x.ToUserId).IsRequired()
                    .OnDelete(DeleteBehavior.NoAction);
                b.HasOne(x => x.Group)
                    .WithMany(gr => gr.Payments)
                    .HasForeignKey(x => x.GroupId);
                b.Property(x => x.Description)
                    .HasMaxLength(50);
                b.Property(x => x.Amount)
                    .HasColumnType("decimal(18,4)");
            });

            modelBuilder.Entity<User>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasMany(x => x.Groups)
                    .WithMany(g => g.Users);
                b.Property(x => x.Name)
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<Group>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasMany(x => x.Users)
                    .WithMany(u => u.Groups);
                b.Property(x => x.Name)
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<ExpenseHeader>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasOne(x => x.Group)
                    .WithMany(g => g.ExpenseHeaders)
                    .HasForeignKey(x => x.GoupId);
                b.HasOne(x => x.User)
                    .WithMany(g => g.ExpenseHeaders)
                    .HasForeignKey(x => x.UserId);
                b.Property(x => x.Description)
                    .HasMaxLength(50);            
            });

            modelBuilder.Entity<ExpenseLine>(b =>
            {
                b.HasKey(x => x.Id);
                b.HasOne(x => x.Expense)
                    .WithMany(g => g.ExpenseLines)
                    .HasForeignKey(x => x.ExpenseId);
                b.HasOne(x => x.User)
                    .WithMany(g => g.ExpenseLines)
                    .HasForeignKey(x => x.UserId).IsRequired()
                    .OnDelete(DeleteBehavior.NoAction); 
                b.Property(x => x.Amount)
                    .HasColumnType("decimal(18,4)");
            });
        }
    }
}
