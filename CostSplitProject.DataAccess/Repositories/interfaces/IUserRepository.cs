﻿using CostSplitProject.Domain.Models;
using System.Threading.Tasks;

namespace CostSplitProject.DataAccess.Repositories.interfaces
{
    public interface IUserRepository:IRepository<User,int>
    {
        public Task<User> GetByNameAsync(string name);
    }
}
