﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Ardalis.Specification;


namespace CostSplitProject.DataAccess.Repositories.interfaces
{
    public interface IRepository<T, TKey> where T : Domain.Interfaces.IEntity<TKey>
    {
        IUnitOfWork UnitOfWork { get; }
        Task<T> GetByIdAsync(TKey id);
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> InsertAsync(T item);
        Task UpdateAsync(T item);
        Task DeleteAsync(T item);
        Task<ICollection<T>> GetManyAsync(ISpecification<T> specification);
        Task<T> GetOneAsync(ISpecification<T> specification);
    }
}
