﻿using System.Threading;
using System.Threading.Tasks;

namespace CostSplitProject.DataAccess.Repositories.interfaces
{
    public interface IUnitOfWork
    {
        Task<int> SaveChangesAsync(CancellationToken token = default);
    }
}