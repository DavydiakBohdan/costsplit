﻿using CostSplitProject.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CostSplitProject.DataAccess.Repositories.interfaces
{
    public interface IPaymentRepository:IRepository<Payment,int>
    {
    }
}
