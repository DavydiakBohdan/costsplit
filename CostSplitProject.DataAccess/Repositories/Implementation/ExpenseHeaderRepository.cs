﻿using CostSplitProject.DataAccess.Context;
using CostSplitProject.DataAccess.Repositories.interfaces;
using CostSplitProject.Domain.Models;

namespace CostSplitProject.DataAccess.Repositories.Implementation
{
    public class ExpenseHeaderRepository : BaseRepository<ExpenseHeader, int, SplitAppContext>, IExpenseHeaderRepository
    {
        public ExpenseHeaderRepository(SplitAppContext context):base(context)
        {

        }
        
    }
}
