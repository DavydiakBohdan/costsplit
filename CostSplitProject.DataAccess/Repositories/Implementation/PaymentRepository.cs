﻿using CostSplitProject.DataAccess.Context;
using CostSplitProject.DataAccess.Repositories.interfaces;
using CostSplitProject.Domain.Models;

namespace CostSplitProject.DataAccess.Repositories.Implementation
{
    public class PaymentRepository : BaseRepository<Payment,int,SplitAppContext>,IPaymentRepository
    { 
        public PaymentRepository(SplitAppContext context) : base(context)
        {
        }
    }
}
