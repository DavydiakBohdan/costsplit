﻿using CostSplitProject.DataAccess.Context;
using CostSplitProject.DataAccess.Repositories.interfaces;
using CostSplitProject.Domain.Models;

namespace CostSplitProject.DataAccess.Repositories.Implementation
{
    public class GroupRepository : BaseRepository<Group, int, SplitAppContext>, IGroupRepository
    {
        public GroupRepository(SplitAppContext context) : base(context)
        {

        }
    }
}
