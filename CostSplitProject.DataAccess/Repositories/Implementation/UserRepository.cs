﻿using CostSplitProject.DataAccess.Context;
using CostSplitProject.DataAccess.Repositories.interfaces;
using CostSplitProject.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace CostSplitProject.DataAccess.Repositories.Implementation
{
    public class UserRepository : BaseRepository<User, int, SplitAppContext>, IUserRepository
    {
        public UserRepository(SplitAppContext context) : base(context)
        {
            
        }

        public async Task<User> GetByNameAsync(string name)
        {
            return await _dbSet.FirstOrDefaultAsync(x => x.Name.Equals(name));
        }
    }
}
