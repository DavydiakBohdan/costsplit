﻿using CostSplitProject.DataAccess.Repositories.interfaces;
using CostSplitProject.BusinessLogic.Services.Implementation;
using Moq;
using System.Threading.Tasks;
using Xunit;
using System.Threading;
using CostSplitProject.BusinessLogic.Exceptions;
using CostSplitProject.Domain.Models;
using Ardalis.Specification;
using System.Collections.Generic;
using CostSplitProject.Specification;

namespace CostSplitProject.BusinessLogic.Tests
{
    public class ExpenseServiceTest
    {
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly Mock<IExpenseHeaderRepository> _repositoryExpenceMock;
        private readonly Mock<IPaymentRepository> _repositoryPaymentMock;
        private readonly Mock<IGroupRepository> _repositoryGroupMock;
        private readonly PaymentService _paymentService;
        private readonly ExpenseService _service;

        public ExpenseServiceTest()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _unitOfWorkMock.Setup(x => x.SaveChangesAsync(It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(1));

            _repositoryExpenceMock = new Mock<IExpenseHeaderRepository>();
            _repositoryExpenceMock.Setup(x => x.UnitOfWork)
                .Returns(_unitOfWorkMock.Object);

            _repositoryGroupMock = new Mock<IGroupRepository>();
            _repositoryGroupMock.Setup(x => x.UnitOfWork)
                .Returns(_unitOfWorkMock.Object);

            _repositoryPaymentMock = new Mock<IPaymentRepository>();
            _repositoryPaymentMock.Setup(x => x.UnitOfWork)
                .Returns(_unitOfWorkMock.Object);

            _paymentService = new PaymentService(_repositoryPaymentMock.Object);

            _service = new ExpenseService(_repositoryExpenceMock.Object , _repositoryGroupMock.Object , _paymentService);
        }

        [Fact]
        public void CalculateDebtAsync_GroupDoesNotExist_GroupDoesNotExistExceptionIsThrown()
        {
            //Arrange
            SetupGetGroupById(null);

            //Act
            //Assert
            Assert.ThrowsAsync<GroupDoesNotExist>(async () => await _service.CalculateDebtAsync(0, 0 ,0));
        }

        [Fact]
        public void CalculateDebtAsync_PaymentAndExpenseIsNull_DebtReturned()
        {
            //Arrange
            var group = new Group
            {
                Id = 0
            };
            SetupGetGroupById(group);

            SetupGetExpense(null);
            
            SetupGetPayments(null);

            //Act
            var debt = new Debt()
            {
                UserId = 0,
                Amount = 0,
                ToUserId = 1
            };
            var result = _service.CalculateDebtAsync(0, 0, 1).Result;
            //Assert
            Assert.Equal(debt.Amount, result.Amount);
        }


        [Fact]
        public void CalculateDebtAsync_GroupPaymentExpenseExist_DebtReturned()
        {
            //Arrange
            var group = new Group
            {
                Id = 0
            };
            SetupGetGroupById(group);

            var expenseLines = new List<ExpenseLine>() 
            { 
                new ExpenseLine() {
                Id = 0,
                ExpenseId = 0,
                UserId = 0,
                Amount = 100
                } 
            };

            var expense = new ExpenseHeader()
            {
                Id = 0,
                ExpenseLines = expenseLines
            };
            SetupGetExpense(new List<ExpenseHeader>() { expense });
            var payments = new List<Payment>()
            {
                new Payment()
                {
                Id = 0,
                ToUserId = 0,
                FromUserId = 1,
                Amount = 150
                }
                ,new Payment() {
                Id = 1,
                ToUserId = 1,
                FromUserId = 0,
                Amount = 200
                }
            };
            SetupGetPayments(payments);

            //Act
            var debt = new Debt()
            {
                UserId = 0,
                Amount = 50,
                ToUserId = 1
            };
            var result = _service.CalculateDebtAsync(0, 0, 1).Result;
            //Assert
            Assert.Equal(debt.Amount, result.Amount);
        }

        

        private void SetupGetGroupById(Group group)
        {
            _repositoryGroupMock.Setup(x => x.GetByIdAsync(It.IsAny<int>()))
                            .Returns(Task.FromResult(group));

        }
        private void SetupGetExpense(ICollection<ExpenseHeader> expense)
        {
            _repositoryExpenceMock.Setup(x => x.GetManyAsync(It.IsAny<ISpecification<ExpenseHeader>>()))
                            .Returns(Task.FromResult(expense));

        }
        private void SetupGetPayments(ICollection<Payment> payments)
        {
            var spec = new PaymentUserConfirmSpecification(0,0);
            _repositoryPaymentMock.Setup(x => x.GetManyAsync(It.IsAny<ISpecification<Payment>>()))
                            .Returns(Task.FromResult(payments));

        }
    }
}
