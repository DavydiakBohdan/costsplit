﻿using AutoMapper;
using CostSplitProject.Domain.Models;
using CostSplitProject.DTO;

namespace CostSplitProject.Profiles
{
    public class AppProfile:Profile
    {
        public AppProfile()
        {
            CreateGroupMap();
            CreateUserMap();
            CreateExpenseMap();
            CreatePaymentMap();
        }

        private void CreatePaymentMap()
        {
            CreateMap<Payment, PaymentGetDto>();
            CreateMap<PaymentPostDto, Payment>();
        }

        private void CreateExpenseMap()
        {
            CreateMap<ExpenseHeader, ExpenseGetDto>();
            CreateMap<ExpensePostDto, ExpenseHeader>();
            CreateMap<ExpenseLine, ExpenseLineDto>();
        }

        private void CreateUserMap()
        {
            CreateMap<User, UserGetDto>();
            CreateMap<UserExpenseDto, User>();
            CreateMap<UserPostDto, User>()
                .ForMember(x => x.Name, opt => opt.MapFrom(l => l.Login));
        }

        private void CreateGroupMap()
        {
            CreateMap<Group, GroupGetDto>();
            CreateMap<GroupPostDto, Group>();
        }
    }
}
