﻿using AutoMapper;
using CostSplitProject.BusinessLogic.Services.Implementation;
using CostSplitProject.BusinessLogic.Services.Interfaces;
using CostSplitProject.Domain.Models;
using CostSplitProject.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CostSplitProject.Controllers
{
    [Route("api/expenses")]
    [ApiController]
    public class ExpenseController : ControllerBase
    {
        private readonly IExpenseService _expenseService;
        private readonly IMapper _mapper;
        public ExpenseController(IExpenseService expenseService, IMapper mapper)
        {
            _expenseService = expenseService;
            _mapper = mapper;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<ExpenseGetDto>> CreateExpenseAsync([FromBody] ExpensePostDto expensePost)
        {
            var expense = _mapper.Map<ExpenseHeader>(expensePost);
            expense.UserId = UserId;
            var users = _mapper.Map<ICollection<User>>(expensePost.Users);
            expense = await _expenseService.CreateExpenseAsync(expense, expensePost.TotalAmount, users);
            var result = _mapper.Map<ExpenseGetDto>(expense);
            return Ok(result);
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<ExpenseGetDto>> GetExpenseAsync(int id)
        {
            var expense = await _expenseService.GetExpenseAsync(id);
            var result = _mapper.Map<ExpenseGetDto>(expense);
            return Ok(result);
        }

        [Authorize]
        [HttpGet("debt")]
        public async Task<ActionResult<Debt>> GetDebtAsync(int toUserId, int groupId)
        {
            var debt =  await _expenseService.CalculateDebtAsync(groupId, UserId, toUserId);
            return Ok(debt);
        }


        private int UserId => int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

    }
}
