﻿using AutoMapper;
using CostSplitProject.BusinessLogic.Services.Interfaces;
using CostSplitProject.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CostSplitProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public AccountController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<UserGetDto>> RegisterUserAsync([FromBody] UserPostDto userPost)
        {
            var user = await _userService.RegisterUserAsync(userPost.Login, userPost.Password);
            var result = _mapper.Map<UserGetDto>(user);
            return Ok(result);
        }



        private int UserId => int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
    }
}
