﻿using AutoMapper;
using CostSplitProject.BusinessLogic.Services.Interfaces;
using CostSplitProject.DataAccess.Repositories.interfaces;
using CostSplitProject.Domain.Models;
using CostSplitProject.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CostSplitProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<UserGetDto>> GetAsync()
        {
            var user = await _userService.GetUserAsync(UserId);
            var result = _mapper.Map<UserGetDto>(user);
            return Ok(result);
        }

        private int UserId => int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
    }
}
