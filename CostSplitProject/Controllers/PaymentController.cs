﻿using AutoMapper;
using CostSplitProject.BusinessLogic.Services.Implementation;
using CostSplitProject.BusinessLogic.Services.Interfaces;
using CostSplitProject.Domain.Models;
using CostSplitProject.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CostSplitProject.Controllers
{
    [Route("api/payments")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentService _paymentService;
        private readonly IMapper _mapper;
        public PaymentController(IPaymentService paymentService, IMapper mapper)
        {
            _paymentService = paymentService;
            _mapper = mapper;
        }


        [HttpPost]
        public async Task<ActionResult<PaymentGetDto>> CreatePaymentAsync([FromBody] PaymentPostDto paymentPost)
        {
            var payment = _mapper.Map<Payment>(paymentPost);
            payment.FromUserId = UserId;
            payment = await _paymentService.CreatePaymentAsyinc(payment);
            var result = _mapper.Map<PaymentGetDto>(payment);
            return Ok(result);
        }

        [HttpPut("Confirm/{paymentId}")]
        public async Task<ActionResult> ConfirmPaymentAsync(int paymentId)
        {
            await _paymentService.ConfirmPaymentAsync(paymentId, UserId);
            return Ok();
        }

        private int UserId => int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
    }
}
