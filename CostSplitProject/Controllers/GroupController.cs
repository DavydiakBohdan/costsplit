﻿using AutoMapper;
using CostSplitProject.BusinessLogic.Services.Interfaces;
using CostSplitProject.Domain.Models;
using CostSplitProject.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CostSplitProject.Controllers
{
    [Route("api/groups")]
    [ApiController]
    public class GroupController : ControllerBase
    {

        private readonly IGroupService _groupService;
        private readonly IExpenseService _expenseService;
        private readonly IPaymentService _paymentService;
        private readonly IMapper _mapper;

        public GroupController(IGroupService groupService, IMapper mapper, IExpenseService expenseService, IPaymentService paymentService)
        {
            _expenseService = expenseService;
            _groupService = groupService;
            _mapper = mapper;
            _paymentService = paymentService;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<GroupGetDto>> CreateGroupAsync( [FromBody] GroupPostDto postGroup)
        {
            var group = _mapper.Map<Group>(postGroup);
            group = await _groupService.CreareGroup(group);
            await _groupService.AddUserAsync(UserId, group.Id);
            var result  = _mapper.Map<GroupGetDto>(group);
            return Ok(result);
        }

        [Authorize]
        [HttpPut("user")]
        public async Task<ActionResult> AddUserAsync([FromBody]GroupPutDto groupPut)
        {
            await _groupService.AddUserAsync(groupPut.UserId, groupPut.GroupId);
            return Ok();
        }

        [Authorize]
        [HttpGet("{id}/users")]
        public async Task<ActionResult<ICollection<GroupGetDto>>> GetAllUsersAsync(int id)
        {
            var users = await _groupService.GetUsersAsync(id);

            var result = _mapper.Map<ICollection<UserGetDto>>(users);
            return Ok(result);
        }

        [Authorize]
        [HttpGet("{groupId}/expenses")]
        public async Task<ActionResult<ICollection<PaymentGetDto>>> GetExpenseInGroupAsync(int groupId)
        {
            var expenses = await _expenseService.GetExpenseForGroupAsync(groupId);
            var result = _mapper.Map<ICollection<ExpenseGetDto>>(expenses);
            return Ok(result);
        }

        [HttpGet("{groupId}/payments")]
        public async Task<ActionResult<ICollection<PaymentGetDto>>> GetPaymensAsync(int groupId)
        {
            var payment = await _paymentService.GetPaymentsInGroupAsync(groupId, UserId);
            var result = _mapper.Map<ICollection<PaymentGetDto>>(payment);
            return Ok(result);
        }

        [HttpGet("{groupId}/payments/confirmed")]
        public async Task<ActionResult<ICollection<PaymentGetDto>>> GetConfirmedPaymensInGroupAsync(int groupId)
        {
            var payment = await _paymentService.GetUserConfirmedPaymentsInGroupAsync(groupId, UserId);
            var result = _mapper.Map<ICollection<PaymentGetDto>>(payment);
            return Ok(result);
        }

        private int UserId => int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
    }
}
