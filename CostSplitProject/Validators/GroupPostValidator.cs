﻿using CostSplitProject.DTO;
using FluentValidation;

namespace CostSplitProject.Validators
{
    public class GroupPostValidator : AbstractValidator<GroupPostDto>
    {
        public GroupPostValidator()
        {
            RuleFor(x => x.Name)
            .Cascade(CascadeMode.Stop)
            .NotEmpty()
            .Length(5, 30);
        }
    }
}
