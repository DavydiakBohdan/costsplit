﻿using CostSplitProject.DTO;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.Validators
{
    public class PaymentPostValidator : AbstractValidator<PaymentPostDto>
    {
        public PaymentPostValidator()
        {
            RuleFor(x => x.Amount)
                .GreaterThan(0);
            RuleFor(x => x.GroupId)
                .NotEmpty();
            RuleFor(x => x.ToUserId)
                .NotEmpty();
            RuleFor(x => x.Description)
                .NotEmpty();
        }
    }
}
