﻿using CostSplitProject.DTO;
using FluentValidation;

namespace CostSplitProject.Validators
{
    public class ExpensePostValidator : AbstractValidator<ExpensePostDto>
    {
        public ExpensePostValidator()
        {
            RuleFor(x => x.Description)
                .NotEmpty();
            RuleFor(x => x.GoupId)
                .NotEmpty();
            RuleFor(x => x.TotalAmount)
                .NotEmpty()
                .GreaterThan(0);
            RuleFor(x => x.Users)
                .NotNull();
        }
    }
}
