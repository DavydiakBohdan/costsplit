﻿using CostSplitProject.DTO;
using FluentValidation;

namespace CostSplitProject.Validators
{
    public class UserPostValidation : AbstractValidator<UserPostDto>
    {
        public UserPostValidation()
        {
            RuleFor(x => x.Login)
            .Cascade(CascadeMode.Stop)
            .NotEmpty()
            .Length(5, 30);
            RuleFor(x => x.Password)
            .Cascade(CascadeMode.Stop)
            .NotEmpty()
            .Length(6, 16);
        }
    }
}
