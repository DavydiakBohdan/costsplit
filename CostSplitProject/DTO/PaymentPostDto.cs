﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.DTO
{
    public class PaymentPostDto
    {
        public string Description { get; set; }
        public decimal Amount { get; set; }

        public int ToUserId { get; set; }
        public int GroupId { get; set; }
    }
}
 