﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.DTO
{
    public class UserGetDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
