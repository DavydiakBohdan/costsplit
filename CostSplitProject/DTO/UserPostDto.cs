﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.DTO
{
    public class UserPostDto
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }
}
