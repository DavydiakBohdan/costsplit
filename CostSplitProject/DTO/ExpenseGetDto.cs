﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.DTO
{
    public class ExpenseGetDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }
        public ICollection<ExpenseLineDto> ExpenseLines { get; set; }
        public UserGetDto User { get; set; }

        public GroupGetDto Group { get; set; }
    }
}
