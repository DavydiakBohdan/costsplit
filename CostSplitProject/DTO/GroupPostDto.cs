﻿using CostSplitProject.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.DTO
{
    public class GroupPostDto
    {
        public string Name { get; set; }
    }
}
