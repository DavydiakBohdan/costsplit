﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.DTO
{
    public class PaymentGetDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }
        public decimal Amount { get; set; }
        public bool Confirmed { get; set; }

        public UserGetDto ToUser { get; set; }

        public UserGetDto FromUser { get; set; }

        public GroupGetDto Group { get; set; }
    }
}
