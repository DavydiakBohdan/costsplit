﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.DTO
{
    public class GroupPutDto
    {
        public int GroupId { get; set; }
        public int UserId { get; set; }
    }
}
