﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.DTO
{
    public class ExpenseLineDto
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
       
        public UserGetDto User { get; set; }
    }
}
