﻿using CostSplitProject.Domain.Models;
using System.Collections.Generic;

namespace CostSplitProject.DTO
{
    public class GroupGetDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<UserGetDto> Users { get; set; }
    }
}
