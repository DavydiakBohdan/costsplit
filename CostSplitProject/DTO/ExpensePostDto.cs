﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CostSplitProject.DTO
{
    public class ExpensePostDto
    {
        public string Description { get; set; }
        public int GoupId { get; set; }
        public decimal TotalAmount { get; set; }
        public ICollection<UserExpenseDto> Users{ get; set; }
    }
}
