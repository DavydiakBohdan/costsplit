﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CostSplitProject.BusinessLogic.Exceptions;

namespace CostSplitProject.Middleware
{
    public class ErrorHendlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHendlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {

            try 
            {
                await _next(context);
            }
            catch (CustomException ex) 
            {
                context.Response.StatusCode = (int)ex.StatusCode;
                await context.Response.WriteAsync(ex.Message);
            }
            catch (Exception)
            {
                context.Response.StatusCode = 500;
                await context.Response.WriteAsync("An unexpected error occurred");
            }

        }
    }
}
